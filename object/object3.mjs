//converting object into array

let obj1 = {
    name: "nitan",
    age: 29,
    
}
/////////////////////////////
// converting above object into arrays

//keys array
console.log(Object.keys(obj1)) //[ 'nitan', 29 ]

//value array
console.log(Object.values(obj1)) //[ 'nitan', 29 ]

// properties array
console.log(Object.entries(obj1)) //[ [ 'name', 'nitan' ], [ 'age', 29 ] ]

