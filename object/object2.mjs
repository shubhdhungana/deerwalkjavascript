let obj1 = {
    name: "nitan",
    age: "29",
    name: "ram",
}

// name, age, name = keys
// nitan, 29, ram = properties

// below name overrides above name
console.log(obj1) // => { name: 'ram', age: '29' }

// duplicate key doesn't exist in object, it just overrides as above name