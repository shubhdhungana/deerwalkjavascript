// simple default value function
let fun1 = (a, b, c=3)=>{
    console.log(a)
    console.log(b)
    console.log(c)
}

fun1(1, 2) // => 1 2 3
