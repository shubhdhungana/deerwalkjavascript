// object destructuring
// since age value is not defined, so age will take its default valie i.e. 39
let {name, age=39, isMarried} = {name: "subh", isMarried: false}
console.log(name)
console.log(age)
console.log(isMarried)
