/*
setTimeout(()=>{console.log("hello")},2000)  TASK THAT takes 5 Sec
			  console.log(1);
*/

setTimeout(()=>{console.log("hello")},2000)  
			  console.log(1); // this console.log task take 5 sec

/*
output
---------
1
hello

---------
working mechanism
---------
- setTimeout 2 sec moves to right sidenode section => 2 sec being passed => moves to memory queue down
- Then 1 moves to js section and it will complete on 5 sec => then being executed
- Then from memory queue on right side, setTimeout moves to left side then being executed


*/