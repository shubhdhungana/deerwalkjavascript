setTimeout(()=>{
    console.log("hello")
}, 2000)

// setTimeout structure setTimeout(function, 2000)
// above delay the console by 2 sec



// setTimeout in detail
//js and browsers are different
 /*
    console.log("a")
  setTimeout(()=>{console.log("hello")}, 2).
  console.log("b")
    
  ==> output is 
  a
  b
  hello

  Because, first a turn in js section then it gets run. Then setTimeout will go to node
  section not js section. Then after 2 sec, It goes down to memory queue.
  Then It gets down in memory queue. Then b gets to js section
  . Then b gets executed. Memory queues setTimeout("hello") moves to js section and gets executed.
    .

    */


















