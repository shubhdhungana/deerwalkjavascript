console.log("a") //a ( -> callstack(js section) -> executes)

setTimeout(()=>{
    console.log("first") //(node -> memory queue at 3rd -> callstack->execute)
}, 5000)

setTimeout(()=>{
    console.log("second") //(node -> memory queue at 2nd -> callstack->execute)
}, 3000)

setTimeout(()=>{
    console.log("third")  //(node -> memory queue at 1st -> callstack->execute)
}, 0)

console.log("b") // b  ( -> callstack(js section) -> executes)

/* 
output
-------
a
b
third
second
first

--------------
working mechanism
------------------
a -> callstack section (to memorize say js section) -> then gets executed
setTimeout(first) -> node section then memory queue section after 5 sec (at third line)
setTimeout(second)-> node section then memory queue section after 3 sec (at second line)
setTimeout(third) -> node section then memory queue section after 0 sec (at first line)
b -> go to callstack section( lets say js section) then gets executed
setTimeout(third) -> go to callstack section( lets say js section) then gets executed since 0 sec
setTimeout(second) -> go to callstack section( lets say js section) then gets executed since 3 sec
setTimeout(first) -> go to callstack section( lets say js section) then gets executed since 5 sec

*/