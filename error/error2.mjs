/*
make a  arrow function that will takes one input and it must throw error if input lenght is less less than 3 with message 
"Input length must be at least 3 character long"
*/

let checkLength = (value)=>{
    if (value.length < 3) {
        let errorMessage = new Error ("Input length must be at least 3 character long")
        throw errorMessage
    }
}
console.log(checkLength("sub")) //=>Error: Input length must be at least 3 character long