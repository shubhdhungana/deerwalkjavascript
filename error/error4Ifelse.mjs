
// error message generator
let ageError = (age)=>{
    if (age < 18) {
        let error = new Error ("You are not eligible to join as you are under 18")
        throw error
    }
}
console.log(ageError(18))