/*

make a arrow function that takes input as age and it will
 throw error if age is less than 18 
 with message ("you are not allow to join team")


*/


let ageChecker = (age)=>{
    if (age < 18) {
        let error = new Error("You are not allowed to join ")
        throw error
    }
}
console.log(ageChecker(17)) //=>Error: You are not allowed to join