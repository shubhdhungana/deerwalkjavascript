"" is falsy other all are truthy
0 is falsy other all are truty


let a = ""||0||"nitan" || "ram"
console.log(a) //nitan

/*
if the value is truthy value while checking in order, it shows the first truthy
value. If value is falsy value, it goes to next value and again searches for another
firth truthy value and so on.
*/

// second case
let a = "hello"||0||"nitan" || "ram"
console.log(a) //hello

/*
if the value is truthy value while checking in order, it shows the first truthy
value. If value is falsy value, it goes to next value and again searches for another
firth truthy value and so on.
*/


// third case
let a = ""||0||null || null
console.log(a) //null

/*
if all values are falsy, last standing value would be output
if the value is truthy value while checking in order, it shows the first truthy
value. If value is falsy value, it goes to next value and again searches for another
firth truthy value and so on.
*/


