// spread operator => wrapper opener (it is used to make new variable)
// rest operator --> takes rest of the value

let a1 = [1, 2, 3]
let a2 = [5, 10, ...a1, 11] // [5, 10, 1, 2, 3, 11]

let [a, ...b] = [1, 2, 3] // a = 1, b = [2, 3]