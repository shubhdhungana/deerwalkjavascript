// rest operator
// ... is rest operator sign
// here ...c is rest operator and it takes rest of the value, it should be
// placed at last 

let fun = (a, b, ...c)=>{
    console.log(a)
    console.log(b)
    console.log(c) // [3, 4, 5]
}

fun(1, 2, 3, 4, 5)

/*
==> output
 1
2
[ 3, 4, 5 ]
*/