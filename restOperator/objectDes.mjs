// object Destructuring

let {name, ...info} = {name:"Subham", age:13, isMarried:false}

console.log(name) // Subham
console.log(info) //{ age: 13, isMarried: false }