// array destructuring

let [a, b, ...c] = [1, 2, 3, 4, 5]

console.log(a) //1
console.log(b) //2
console.log(...c) //3 4 5