/*
Question: Use map to double each element in an array.
Example: doubleArray([1, 2, 3]) should return [2, 4, 6].
*/

let arr = [1, 2, 3]

let mapDouble = arr.map((value, i)=>{
    return value*2
})
console.log(mapDouble) //[ 2, 4, 6 ]