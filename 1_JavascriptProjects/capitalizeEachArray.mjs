
let  capitalizeNames = (arr)=>{
    let arrLoopCapitalize = arr.map((value, i)=>{
        return (value.toUpperCase())
    })
    return arrLoopCapitalize
  }
  
  console.log(capitalizeNames(["john", "JACOB", "jinGleHeimer", "schmidt"])); // ["John", "Jacob", "Jingleheimer", "Schmidt"]
