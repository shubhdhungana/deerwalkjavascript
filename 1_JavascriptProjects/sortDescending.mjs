/*
make a arrow function that take input
 as array and return , output in desending order

*/

let descendingArrow = (value)=>{
    return (value.sort().reverse())
}

console.log(descendingArrow(["a", "b", "c"])) //[ 'c', 'b', 'a' ]
