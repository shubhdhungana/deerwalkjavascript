// make palindrome checker function

let palindromeChecker = (word)=>{
    /*
    converting into array using split, reversing it
    and joining it
    */
    let reversedWord = word.split("").reverse().join("")
    if (reversedWord === word) {
        return ("The word is palindrome word")
    }
    else {
        return ("The word is not palindrome word")
    }
}

console.log(palindromeChecker("radar")) //=>The word is palindrome word


