// capitalize second letter of each word

// first part, converting word into capitalized 2nd position word

let secondLetterCap = (word)=>{
    let wordArr = word.split("") //converting into arr => ["s", "u", "b", "h", "a", "m"]
    let wordArrLoop = wordArr.map((value, i)=>{
        if (i === 1) {
            return (value.toUpperCase())
        }else {
            return (value.toLowerCase())
        }
    })
    let wordArrJoint = wordArrLoop.join("")
    return (wordArrJoint)
}


// second part, looping through every word and making it capitalized via first func

let finalCapitalization = (sentence)=>{
    let sentenceArr = sentence.split(" ")
    let sentenceArrLoop = sentenceArr.map((value, i)=>{
        return (secondLetterCap(value))
    })
    
    return (sentenceArrLoop.join(" ")) // cApitalized sEcond iNdex oF eVery wOrd
}
console.log(finalCapitalization("capitalized second index of every word"))
// cApitalized sEcond iNdex oF eVery wOrd