// squaring an array of number

let num1 = [1, 2, 3, 4, 5]

let squaredNumber = num1.map((value, i)=>{
    return (value*value)
})
console.log(squaredNumber) // [ 1, 4, 9, 16, 25 ]