
/* 
// uppercase and lower case transformation

let name = "subHaM"

console.log(name.length)
console.log(name.toUpperCase())
console.log(name.toLowerCase())


 */


// trimstart, trimend, trim
/* 
let name = "   raju    "
console.log(name.trimStart().length);
console.log(name.trimEnd().length);
console.log(name.trim().length);

 */


/* // includes method
let name = "my name is suniti karkee"

console.log(name.includes(" karkee"))
 */


/* 
// includes method
let name = "my name is suniti karkee"

console.log(name.startsWith("my"))
console.log(name.endsWith("karkee "))

 */
// replaces method
/* 
let intro = "hello this is this laptop"

console.log(intro.replaceAll(" ", ",")) // this replaces everything
console.log(intro.replaceAll("this", "thissa")) // replaces every

console.log(intro.replace("this", "thissa")) // replace only first


 */