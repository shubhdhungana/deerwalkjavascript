// array, object (are non primitive)

console.log(undefined === undefined) //true
console.log(null === null) // true
console.log([1,2]===[1,2]) // false since no varialbe container is being
     // allocated and also it should be on the same container address to be true

     
// the type of non primitive is object

let a = [1,2]
let b = [1,2]
let c = a
console.log(a === b) // false, since variable location is not same
console.log(a === c) // true, since variable location is  same

c = [1,2]
console.log(c) // => [ 1, 2 ] // now c is being created to another variable location
console.log(a) // => [ 1, 2 ] // a and c are in different address
console.log(a===c) // false
let d = b
console.log(d) // [1,2] but b and d are in same reference address
console.log(b===d) // true since both in same variable reference address 

// [1,2] = [1,2] // false since no variable address or anything here

/* 

let a = [1,2,3] 
let b = [1,2,3]
let c = a 
console.log(a === c) // true since same variable location
a.push(4)
console.log(c) // [1,2,3,4] Since a and c are in same location and a and c both gets updated
console.log(a==c) // true, since both in same location and both gets updated
c.push(5) // [1,2,3,4,5]
console.log(a) //[ 1, 2, 3, 4, 5 ] Since a & c both in same location both gets updated
c = [1,2,3,4,5,6] // now c variable location is different cuz of this
console.log(c)  //[1,2,3,4,5,6]
console.log(a) //[1,2,3,4,5]
console.log(a ===c) // false */