/// null and undefined
let a;
console.log(a) // undefined
 // undefined since varialbe is defined and untouched

a = 1;
//a = undefined; // wrong practice
a = null
console.log(a) //null
 // null means  variable is defined and then initialized to null

