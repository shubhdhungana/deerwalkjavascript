// slice
// slice only return
// ==> remember technique: slice drink returning to mouth after its droplet fall in  jaw
// slice returns but splice make changes to original array

let list = ["a", "b", "c", "d", "e"]

let list2 = list.slice(1, 3)
console.log(list2) // [ 'b', 'c' ]
console.log(list) // [ 'a', 'b', 'c', 'd', 'e' ] ==> doesn't make changes to original array


// if last index is not given, it prints all rest of the element
console.log(list.slice(1,)) //[ 'b', 'c', 'd', 'e' ]
console.log(list) //[ 'a', 'b', 'c', 'd', 'e' ] => doesn't change original arr
