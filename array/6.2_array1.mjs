// array  basic

let names = ["hello1", 1023, "ram", true, "shyam"]

// indexing
console.log(names)
console.log(names[0])
console.log(names[1])


//  changing array element
names[0] = "rahul"
names[3] = "hello"
console.log(names)

// deleting element
delete names[1] 
console.log(names)
console.log(names[1])
