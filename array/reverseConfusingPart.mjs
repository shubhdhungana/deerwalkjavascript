// reverse method can  make changes to original array

let ar5 = [1,2,3]
let ar6 = ar5.reverse() // trying to return
console.log(ar5) //=> [3, 2, 1]
// reverse method can also return things via variable just like map, filter
/* hence reverse can return value and also do make changes to original. 
array. But, push pop unshift shift cannot return value but can make changes
to original array */