// push, pop, unShift, shift
// below all methods make change to original array


let arr=[9, 10, 11]
// push (add backward in array)
arr.push(5) 
// it changes original arr and gives below result
console.log(arr) //[ 9, 10, 11, 5 ]

// pop method (remove backward element in array)
arr.pop() 
// above pop changes original arr i.e. [9, 10, 11, 5] and gives below result
console.log(arr) //[ 9, 10, 11 ]

// unshift method (add forward in array)
arr.unshift("ram")
console.log(arr) //[ 'ram', 9, 10, 11 ]

// shift (remove forward element in array)
arr.shift()
console.log(arr) //[ 9, 10, 11 ]

// reverse
let ar4 = [6,7,8]
ar4.reverse()
console.log(ar4) //=> [8, 7, 6]

