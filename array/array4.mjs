/*

let myAlphabet = ['A', 'B', 'C', 'D','E','F', 'G'];

    What is the length of the array?
    Write a function called myAlphabetLength which console.logs the length of the array
    Within the function also use an if-conditional statement that checks if the number of items within the array are less than 4


*/
let myAlphabet = ['A', 'B', 'C', 'D','E','F', 'G']


let myAlphabetLength = (value)=>{
    

    if (value.length < 4) {
        return ("It is less than 4")
    }
    else {
        return ("It is more than 4")
    }

}

console.log(myAlphabetLength(myAlphabet))
 //It is more than 4
