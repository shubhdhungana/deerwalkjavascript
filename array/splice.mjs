// splice
// splice change original array but slice returns
/*remember technique: since extra 'p' is there in splice word,
so it contains splice(return feature) + p(making changing to original array)
*/
let list = ["a", "b", "c", "d", "e"]

let splicedList = list.splice(1, 3) // from position 1, remove 3 elements
console.log(list) // [ 'a', 'e' ] => changes to original array
console.log(splicedList) // [ 'b', 'c', 'd' ] ==> selected removed element


/// splice eg 2

let arr = ["a", "b", "c", "d"]
let spliceUpdateAdd = arr.splice(1, 2, "bhen", "chod") // splice and update words
console.log(arr) // =>[ 'a', 'bhen', 'chod', 'd' ] // changed to original arr
console.log(spliceUpdateAdd) // => [ 'b', 'c' ] // selected element




