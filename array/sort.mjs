// sort method
// sort method return and change original array
// sorting only works in string but right now it doesn't work in number sort
// first capital letter is being sorted then small letter
// this is ascending  sort (from small to sorting bigger)
// ["c", "b", "a"] => ["a", "b", "c"]

let ar1 =  ["c", "b", "a"]
let ar2 = ar1.sort() 
console.log(ar2) // => [ 'a', 'b', 'c' ] since sort returns
console.log(ar1) // => [ 'a', 'b', 'c' ] since sort changes original array

// descending sort
let randomLetters = ["b", "a", "d", "c"]
// first sort accendiingly then reverse it, baam it is descending order

console.log(randomLetters.sort().reverse()) //[ 'd', 'c', 'b', 'a' ]


