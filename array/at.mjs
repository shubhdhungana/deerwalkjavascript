


let ar1 = ["a", "b", "c"]
//          0    1     2
//           -3    -2     -1
let value = ar1.at(-1) // opposite indexing
console.log(value) // c


let ar2 = [1, 3, 6]
console.log(ar2.at(-3)) //1 // opposite indexing