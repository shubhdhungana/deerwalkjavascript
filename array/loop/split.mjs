// use to convert string into array
// replaces " " with ,
/* 
let description = "my name is nitan"

let desArr = description.split(" ")
console.log(desArr)

// output [ 'my', 'name', 'is', 'nitan' ]

 */
///////////////////////////


// split eg 2
// split with d, replaces d with comma

/* 
let description = "asdfasdfkjlhkjhdaa"

let desArr = description.split("d")
console.log(desArr)
 */
//////////////////////////////

// split eg 3
// split with empty string, it results every letter comma
/* 
let description = "asdfasdfkjlhkjhdaa"

let desArr = description.split("")
console.log(desArr)
 */

// below code splits nitan into array [n, i, t, a, n]
// and then converting array into uppercase 

let name1 = "nitan"

let name2 = name1.split("")
console.log(name2)

let name3 = name2.map((value, i)=>{
    let display = value.toUpperCase(value)
    return (`${display}${display}`)
})

console.log(name3)


