/*
Question: Write a function to filter even numbers from an array.
Example: filterEven([1, 2, 3, 4, 5]) should return [2, 4].
*/

let numbers = [1, 2, 3, 4, 5]
let filterEven = numbers.filter((value,i)=>{
    if (value % 2 === 0) {
        return (true)
    }else{
        return false
    }
})
console.log(filterEven) //[ 2, 4 ]