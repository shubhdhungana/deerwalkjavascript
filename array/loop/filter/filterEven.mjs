let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

let oddNumberFilter = numbers.filter((value, i)=>{
    if (value % 2 !== 0) {
        return true
    }
})

console.log(oddNumberFilter) // [ 1, 3, 5, 7, 9 ]