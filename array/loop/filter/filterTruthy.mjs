// filter only truthy value
let arTruthy = ["", " ", 0, 1, 3, "something"]

let filterTruthy = arTruthy.filter((value, i)=>{
    if (value !== "" || value !== 0) {
        return true
    }
    
})
console.log(filterTruthy)