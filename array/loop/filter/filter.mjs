
// filtering out even number
// it either returns true (pass garney) or return false(nagarney)

let ar1 = [1, 2, 3, 5, 6, 7]

let ar2 = ar1.filter((value, i)=>{
    if (value % 2 === 0) {
        return (true)
    }
    else {
        return (false)
    }
})
console.log(ar2)


///////////////////////////////////

// filter greater than 17
let greaterThanFilter = [1, 2, 3, 5, 17, 18, 19, 20]

let greaterThan17 = greaterThanFilter.filter((value, i)=>{
    if (value > 17) {
        return (true)
    }
    else {
        return (false)
    }
})

console.log(greaterThan17)


// filter greater than 4 length word
let names = ["nitan", "ram", "hari1"]

let namesFiilter = names.filter((value, i)=>{
    if (value.length > 4) {
        return (true)
    }
    else {
        return (false)
    }
})
console.log(namesFiilter)


/// filter positive number
let number = [-1, -2, -3, 4, 5, 6, 9]

let positiveNumber = number.filter((value, i)=>{
    if (value > 0) {
        return (true)
    }
    else {
        return (false)
    }
})
console.log(positiveNumber)
////////////////////

// filter string ["a",1,"b",3,"nitan",] = ["a","b","nitan"] (filter the string)

let filterString = ["a",1,"b",3,"nitan"]

let filterString2 = filterString.filter((value, i)=>{
    if (typeof (value) ==="string"){
        return (true)
    }
})
console.log(filterString2)


// filter 10 to 100
let number10 = [1, 2, 3, -10, 100, 1320, 38, 38, 10, 11]

let numberFilter10to100 = number10.filter((value, i)=>{
    if (value >= 10 && value <= 100) {
        return true
    }
})
console.log(numberFilter10to100)


// filter only truthy value
let arTruthy = ["", " ", 0, 1, 3, "something"]

let filterTruthy = arTruthy.filter((value, i)=>{
    if (value === "" || value === 0) {
        return false
    }
    else {
        return true
    }
})
console.log(filterTruthy)