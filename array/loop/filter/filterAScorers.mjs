// a scorer filter
// filter out the A+ scorers and the total students scoring it

let numbers = [64, 38, 23, 64, 80, 33, 98, 89, 80, 84]

let scorersA = numbers.filter((value, i)=>{
    if (value > 80) {
        return (true)
    }
    else {
        return (false)
    }
})
console.log(scorersA) // [ 98, 89, 84 ]
console.log(`Total numbers of students ${scorersA.length}`)
// Total numbers of students 3
