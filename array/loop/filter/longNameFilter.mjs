// filter out the long names greater than length 6

let names = ["rohan", "subha", "suniti", "shubham", "sarthak", "saksham"]

let filterNames = names.filter((value, i)=>{
    if (value.length >= 6) {
        return true
    }
})

console.log(filterNames) // [ 'suniti', 'shubham', 'sarthak', 'saksham' ]