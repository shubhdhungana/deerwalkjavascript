// convert every lowercase word into uppercase

let lowerCaseWord = ["this", "is", "my", "school"]

let upperCaseConverter = lowerCaseWord.map((value, i)=>{
    return (value.toUpperCase())
})

console.log(upperCaseConverter)

// convert ["a", "b", "c"] = ["aA", "bB", "cC"]

let arrManipulation = ["a", "b", "c"]
let mapWordRepeatManipulation = arrManipulation.map((value, i)=>{
    return (value+value.toUpperCase())
})
console.log(mapWordRepeatManipulation)

