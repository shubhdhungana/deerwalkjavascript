/* let ar1 = ["a", "b", "c"]
// join method converts array into string
// below code joins array with space

let ar2 = ar1.join(" ") // a b c
console.log(ar2)


 */


// join method to convert arr into string using empty string
/* let ar1 = ["a", "b", "c"]
let ar2 = ar1.join("*")
let ar3 = ar1.join("")

console.log(ar2) //a*b*c
console.log(ar3) //abc


 */

/* 
// change "subham" to "ssuubbhhaam"


// subham into [s, u, b, h, a, m] ==> split

let name1 = "subham"
let arrName = name1.split("")
console.log(arrName) // output [ 's', 'u', 'b', 'h', 'a', 'm' ]

// arr manipulation using map method

let doubleName = arrName.map((value, i)=>{
    return (`${value}${value}`)
})
console.log(doubleName) // ==> [ 'ss', 'uu', 'bb', 'hh', 'aa', 'mm' ]

// changing back to string
console.log(doubleName.join("")) // output => ssuubbhhaamm

 */
////////////////////////////////////////////////

// map join split exercise

let arr = ["my", "name", "is", "rohan"]
let uppercaseMakerArray = arr.map((value, i)=>{
    return (value.toUpperCase())
})
console.log(uppercaseMakerArray) // => [ 'MY', 'NAME', 'IS', 'ROHAN' ]
//joining it 
let joinedString = uppercaseMakerArray.join(" ")
console.log(joinedString) // => MY NAME IS ROHAN

// split => array
let arrayString = joinedString.split(" ")
let lenArrayString = arrayString.length
console.log(lenArrayString) // 4

//manipulation array
let doublingArray = uppercaseMakerArray.map((value,i)=>{
    return(`${value}${value}`)
})
console.log(doublingArray)  // => [ 'MYMY', 'NAMENAME', 'ISIS', 'ROHANROHAN' ]