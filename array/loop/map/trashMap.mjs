// write a program to make every word's first letter capital

// first chunk to make only word first letter capitalized
let justWordCapitalizer = (aWord)=>{
    let arrWord = aWord.split("") // ["r", "a", "m"]
    let loopLetter = arrWord.map((value, i)=>{
        if (i === 0) {
            return (value.toUpperCase())
        }else {
            return (value.toLowerCase())
        }
            
        // => ["R", "a", m]
    }) 
    return (loopLetter.join(""))
}


// second chunk to loop through every word then integrated 1st function to work
let everyWordCapitalizer = (sentence)=>{
    let sentenceArr = sentence.split(" ")
    let sentenceLoop = sentenceArr.map((value, i)=>{
        return (justWordCapitalizer(value))
    })
    return (sentenceLoop.join(" ")) //This Will Capitalize Every Word First Letter
}
console.log(everyWordCapitalizer("this will capitalize evERY wOrd first lEtTer"))