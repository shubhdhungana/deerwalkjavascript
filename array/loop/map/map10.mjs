/*
Write a function capitalize that takes a string
 and uses .map to return the same string in all caps.
*/

let capitalize = (string)=>{
    let arrString = string.split(" ") //arr conversion
// looping through array via map method
    let capitalMaker = arrString.map((value, i)=>{
        return (value.toUpperCase()) 
    })
    let joinedString = capitalMaker.join (" ")
    return (joinedString) // joining arr and converting into string

    
}

console.log(capitalize("oh hey hi")) 
// ==> OH HEY HI
