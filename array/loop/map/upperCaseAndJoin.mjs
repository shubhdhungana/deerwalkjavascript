// make every word, a uppercase and convert into

let description = "my name is rajeev mahato"
//converting it into arr using split
let arrDesc = description.split(" ")
console.log(arrDesc) //[ 'my', 'name', 'is', 'rajeev', 'mahato' ]
// converting using uppercase using map
let upperCaseMaker = arrDesc.map((value, i)=>{
    return (value.toUpperCase())
})
console.log(upperCaseMaker) // [ 'MY', 'NAME', 'IS', 'RAJEEV', 'MAHATO' ]
//join using space
console.log(upperCaseMaker.join(" ")) //MY NAME IS RAJEEV MAHATO