/* 
let ar1 = [1, 2, 3]

// syntax, execution map method
let ar2 = ar1.map((value, i)=>{
    return value
})

console.log(ar2)


 */

/* 
// map method of output twice array

let ar1 = [1, 3, 5]

let ar2 = ar1.map((value, i)=>{
    return value * 2
})

console.log(ar2)

 */

// simple map
/* let ar1 = [9, 10, 11]

let ar2 = ar1.map((value, i)=>{
    return value * i
})

console.log(ar2)

 */



// map to print [100, 200, 300]
/* 
let ar1 = [1, 2, 3]

let ar2 = ar1.map((value, i)=>{
    let output = value * 100
    return (output)
})

console.log(ar2)


 */


// map to print [3, 6, 9]
let ar1 = [1, 2, 3]
            
// ar2 = [1, 2, 2]

export let ar2 = ar1.map((value, i)=>{
    return (value)
})

console.log(ar2)




















