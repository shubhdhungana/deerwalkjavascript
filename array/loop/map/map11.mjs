/*

    console.log each item in this array WITHOUT using a for loop

   let myArr = [ 1, 2, 'One', true];

*/

let myArr = [ 1, 2, 'One', true]

let everyElement = myArr.map((value, i)=>{
    return (value)
})
console.log(everyElement) //[ 1, 2, 'One', true ]