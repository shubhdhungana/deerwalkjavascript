

// function for lastindex capital maker
let lastLetterCap = (word)=>{
    // word to array
    let wordArr = word.split("") // ["c", "a", "z", "u"]
    //looping on the arr
    let loopArr = wordArr.map((value, i)=>{
        
        if (i === wordArr.length-1) { // or , if (value === wordArr.at(-1))
            return (value.toUpperCase())
        }else {
            return(value.toLowerCase())
        }
    })
    return (loopArr.join("")) // => raM
}

// looping through every word in a sentence and implementing above function
let wholeSentenceCap = (sentence)=>{
    // making arr
    let senArr = sentence.split(" ")
    
    // loop
    let senLoop = senArr.map((value, i)=>{
        return (lastLetterCap(value))
    })
    return (senLoop.join(" ")) // thiS makeS everY worD lasT letteR capitalizeD

}
console.log(wholeSentenceCap("this makes every word last letter capitalized"))
// thiS makeS everY worD lasT letteR capitalizeD