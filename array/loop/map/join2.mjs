// convert "nitan" into "NNINTANN"

let word1 = "nitan"

// spliting string into array
let wordArr = word1.split("")
console.log(wordArr) //[ 'n', 'i', 't', 'a', 'n' ]

// adding extra N
let addN = wordArr.map((value, i)=>{
    return (`${value.toUpperCase()}N`)
})
console.log(addN) //[ 'NN', 'IN', 'TN', 'AN', 'NN' ]

// joining the arr and making it to string
// converting to string
let stringResult = addN.join("")
console.log(stringResult) // NNINTNANNN

