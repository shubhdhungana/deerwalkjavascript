// capitalizing first letter of every word

// first part, making 1st function for capitizing a letter in one word

let oneWordCapitalizer = (word)=>{
  let wordArr = word.split("") // ["r", "a", "m"]
  
  let firstLetterCap = wordArr.map((value, i)=>{
    if (i===0) {
      return (value.toUpperCase())
    }else {
      return (value.toLowerCase())
    }
  }) // ==> ["R", "a", m]

  let finalWord = firstLetterCap.join("")
  return finalWord
}

//next chunk part
// putting above function in final map loop

let sentenceCapitalizer = (sentence)=>{
  let arrSentence = sentence.split(" ")
  let sentenceLoop = arrSentence.map((value, i)=>{
    return (oneWordCapitalizer(value))
  })
  let finalSentence = sentenceLoop.join(" ")
  return finalSentence
}

console.log(sentenceCapitalizer("helmets nepal is located in kathmandu")) // This Is Subh Dhungana