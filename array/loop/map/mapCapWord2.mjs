
// this chunk of function work is to make first letter of word capital
let firstLetterCapital = (input) => {
    //input = "my" => ["m", "y"] =>["M", "y"] ="My"

    let inputAr = input.split("") ["m", "y"]
    inputAr[0] = inputAr[0].toUpperCase()
    console.log(inputAr) // ["M", "y"]

    let output = inputAr.join("");
    return output;
}
// this is second part and integrating above function in below code
let isWordCapital = (input) => {
    // input = "my name is nitan"

    let inputAr = input.split(" ") // ["my", "name", "is", "nitan"]
    //outputAr =["my", "name", "is", "nitan"]


    let outputAr = inputAr.map((value, i)=>{
        return (firstLetterCapital(value)); 
    })
    let output = outputAr.join(" ")
    return output;
}
console.log(isWordCapital("my name is nitan")) // My Name Is Nitan