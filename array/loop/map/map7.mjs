// convert [1, 2, 3, 4, 6, 7] = [100, 0, 300, 0, 0, 700]
// if odd, * 100 else 0

let numbArr = [1, 3, 3, 4, 6, 7]

let oddEvenMap = numbArr.map((value, i)=>{
    if(value % 2 === 0) {
        return value*100
    }
    else {
        return (0)
    }
})
console.log(oddEvenMap) //[ 0, 0, 0, 400, 600, 0 ]