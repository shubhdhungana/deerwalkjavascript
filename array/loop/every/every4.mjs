/*
check whether all students get pass mark from the list [ 40,30,80]
 here pass marks is 40
*/

let marks = [40, 63, 83, 40, 59]

let passOrNot = marks.every((value, i)=>{
    if (value > 39) {
        return (true)
    }
})

console.log(passOrNot)