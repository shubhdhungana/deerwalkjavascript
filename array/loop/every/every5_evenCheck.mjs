// check if all elements in ar1 are even.

let ar1 = [1, 3, 5, 4, 5, 8, 9]

let evenChecker = ar1.every((value, i)=>{
    if (value % 2 === 0) {
        return (true)
    }
})

console.log(evenChecker) // true