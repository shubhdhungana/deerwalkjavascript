// check whether we have all even number in the list [2, 4 9, 6]

let num = [2, 4, 9, 6]

let numEven = num.every((value, i)=>{
    if (value % 2 === 0) {
        return true
    }
})

console.log(numEven) // false