/*

                make a arrow function
                 that take input as [1,2,3] , and return [1,2,3,4] by using push
                  methode
                */

let arrPush = (value)=>{
    value.push(4)
    return (value) // [ 1, 2, 3, 4 ]
}
console.log(arrPush([1, 2, 3])) // [ 1, 2, 3, 4 ]


/*
make a arrow function which takes input as [1,2,3] and produce output as "1,2,3
*/

let arrRemover = (arrR)=>{
    
    return (arrR.join(",")) //1,2,3
}
console.log(arrRemover([1,2,3]))


