// reverse
// this method return array too and also make changes to original array
// ["a", "b", "c"] ==> ["c", "b", "a"]

let ar1 = ["a", "b", "c"]

let ar2 = ar1.reverse()
console.log(ar2) //[ 'c', 'b', 'a' ]
console.log(ar1) // [ 'c', 'b', 'a' ]

/* previously ar1 was different now, its c,b,a because reverse method
changes original array */

