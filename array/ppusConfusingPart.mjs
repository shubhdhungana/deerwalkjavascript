
//////////////////////////////
let ar1 = [1, 3, 6]
let ar2 = ar1.push(4)
console.log(ar2) // =>4
console.log(ar1) // => [ 1, 3, 6, 4 ]

/* the result is 4 because push pop unshift shift doesn't return via
    variable ar2 but when ar1 is being consoled after making push,
    it do make changes to original array but it doesn't return via
    ar2.

    Hence push pop unshift shift doesn't return but make changes
    to original array
*/

// it should be like this
let ar3 = [3, 5, 6]
ar3.push(7)
console.log(ar3) // =>[ 3, 5, 6, 7 ]
// ppus method do make changes in original array

///////////////////////