
// filtering out even number
// it either returns true (pass garney) or return false(nagarney)

let ar1 = [1, 2, 3, 5, 6, 7]

let ar2 = ar1.filter((value, i)=>{
    if (value % 2 === 0) {
        return (true)
    }
    else {
        return (false)
    }
})
console.log(ar2)


///////////////////////////////////

// filter greater than 17
let greaterThanFilter = [1, 2, 3, 5, 17, 18, 19, 20]

let greaterThan17 = greaterThanFilter.filter((value, i)=>{
    if (value > 17) {
        return (true)
    }
    else {
        return (false)
    }
})

console.log(greaterThan17)


// filter greater than 4 length word
let names = ["nitan", "ram", "hari1"]

let namesFiilter = names.filter((value, i)=>{
    if (value.length > 4) {
        return (true)
    }
    else {
        return (false)
    }
})
console.log(namesFiilter)


/// filter positive number
let number = [-1, -2, -3, 4, 5, 6, 9]

let positiveNumber = number.filter((value, i)=>{
    if (value > 0) {
        return (true)
    }
    else {
        return (false)
    }
})
console.log(positiveNumber)

