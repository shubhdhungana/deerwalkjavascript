// converting to upper case function
/* 
export let convertUpper = (value)=>{
    return (value.toUpperCase())
}
 */

//////////////////////////////

/* 
// converting to lowercase

export let convertLower = (value)=>{
    return (value.toLowerCase())
}

 */
/* 
export let trimBoth = (value)=>{
    return (value.trim())
}

 */

/* 
// make a arrow function if else, if  "hello"  return true, else false

export let beginWith = (value)=>{
    if(value.startsWith("hello")) {
        return (true)
    }
    else {
        return (false)
    }
}

 */

/* 
export let convertArrow = (value)=>{
    return (value.replace("nitan","Ram"))
}


export let adminSuperAdmin = (value)=>{
    if (value.includes("admin")) {
        return true
    }
    else if (value.includes("superAdmin")) {
        return true
    }
    else {
        return false
    }
}


export let lowerCaseArrow = (value)=>{
    return (value.toLowerCase())
}


// length arrow function
export let lengthArrow = (value)=>{
    return (value.length)
}

// uppercase function
export let convertToUpper = (value)=>{
  return (value.toUpperCase(value))
}


 */
/* 
// upperCase  function
export let upperCase = (value)=>{
    return (value.toUpperCase())
}

// lowerCase function
export let lowerCaseFunction = (value)=>{
    return (value.toLowerCase(value))
}

export let convertWord = (value)=>{
    return (value.replace("go to hell", "unethical"))
}

export let trimWord = (value)=>{
    return (value.trim(value))
}


export let wordSelector = (value)=>{
    if (value.includes("Bearear")) {
        return true
    }
    else {
        return false
    }
}


export let convertWordAll = (value)=>{
    return(value.replaceAll("nitan", "ram"))
}


 */








