/* let name = "hello"
name = "hello"
console.log(name) //=>hello
 */

const name = "nitan"
name = "ram"
console.log(name) //  we cannot change const variable, so its error

// above let variable can re assigned but const can't be reassigned