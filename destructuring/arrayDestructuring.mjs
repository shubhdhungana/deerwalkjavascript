// destructuring

let [a, b, c] = [1, 2, 3]
console.log(a) // 1
console.log(b) //2
console.log(c) //3

/* let variable 
console.log(variable) // undefined
// undefined means variable defined but value not given */