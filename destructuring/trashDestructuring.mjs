// array destructuring

let [a, b, c] = [1, 3, 5]
console.log(a)
console.log(b)
console.log(5)

// object destructuring

let classInfos = {
    benchNum: 12,
    studentNumb: 34,
    Motto: "big person",

}
 // object destructuring practise
let {benchNum, studentNumb, Motto} = {
    benchNum: 12,
    studentNumb: 34,
    Motto: "big person",
}
console.log(benchNum) //12
console.log(studentNumb) //34
console.log(Motto) // big person