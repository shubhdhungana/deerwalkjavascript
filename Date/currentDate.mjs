// date

console.log(new Date()) 

// yyyy-mm-mmTss (it should be in the same format like 4 digit in year, 2 digit in month)
// 2024-01-05T10:38:23.409Z // iso format


// vvi (remember this to avoid error)
// must have 4 digit in year
// 2 digit in month

// date an time in readable format
let dateTime = new Date().toLocaleString() // toLocaleString fetch the location of string
console.log(dateTime) // 1/5/2024, 4:32:59 PM (in readable format)


// another for fetching date
let date = new Date().toLocaleDateString() // it fetches date only
console.log(date) //1/5/2024

// fetching time
let time = new Date().toLocaleTimeString() // fetching time
console.log(time) //4:34:57 PM