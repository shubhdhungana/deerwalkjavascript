/* // function to print every letter first word capital

// first function to make one word first indexed letter capital
let firstWordCapitalizer = (word)=>{
    let wordArr = word.split("") // ['r', 'a', 'm']
    let wordArrLoop = wordArr.map((value, i)=>{
        if (i === 0) {
            return (value.toUpperCase())
        }
        else {
            return (value.toLowerCase())
        }
    })
    return wordArrLoop.join("")
}
console.log(firstWordCapitalizer("ram"))


// final function to loop through every word and make capital
let everyWordCapitalizer = (sentence)=>{
    let sentenceArr = sentence.split(" ") // ["this" "is"]
    let sentenceArrLoop = sentenceArr.map((value, i)=>{
        return (firstWordCapitalizer(value))
    })
    return (sentenceArrLoop.join(" "))
}
console.log(everyWordCapitalizer("This makes every word first letter capital")) 


 */



///////////////////////////


/* // function 18 checker or not

let ageChecker = (age)=>{
    if (age >= 18) {
        return ("You can enter")
    }else {
        return ("You cannot enter")
    }
}
console.log(ageChecker(17))


 */

/* 
// name display

let nameDisplay = (name)=>{
    return (name)
}

console.log(nameDisplay("hello"))

 */

//fast palindrome checker
/* 
let palindromeChecker = (word)=>{

    let reversedWord = word.split("").reverse().join("") // arr->reverse->string
    if (reversedWord === word) {
        return ("this is palindrome word")
    }else {
        return ("this is not palindrome")
    }
}
console.log(palindromeChecker("helleh"))




 */
/* 
// palindrome checker
let palindromeChecker = (word)=>{
    let reversedWord = word.split("").reverse().join("")
    if (word === reversedWord) {
        return ("The word is palindrome word")
    }else {
        return ("The word is not palindrome word")
    }
}

console.log(palindromeChecker("helloworld"))

 */

// first indexed letter capitalizer in every word in sentence

/* // doing 1 word 
let firstWordCapitalizer = (word)=>{
    let arrWordLoop = word.split("").map((value, i)=>{
        if(i === 0) {
            return (value.toUpperCase())
        }else {
            return (value.toLowerCase())
        }
    })
    return arrWordLoop.join("")
}
console.log(firstWordCapitalizer("ram"))

// every word
let everyWordCapitalizer = (sentence)=>{
    let sentenceArrLoop = sentence.split(" ").map((value, i)=>{
        return (firstWordCapitalizer(value))
    })
    return sentenceArrLoop.join(" ")
}
console.log(everyWordCapitalizer("every word will be capitalized at first index"))
 */

// Take an array of numbers and make them strings
/* 
let arrNumIntoStrings = (arr)=>{
    let arrIntoString = arr.map((value, i)=>{
        return (value+"")
    })
    return arrIntoString
}
console.log(arrNumIntoStrings([1, 2, 3])) //[ '1', '2', '3' ]


let  capitalizeNames = (arr)=>{
    let arrLoopCapitalize = arr.map((value, i)=>{
        return (value.toUpperCase())
    })
    return arrLoopCapitalize
  }
  
  console.log(capitalizeNames(["john", "JACOB", "jinGleHeimer", "schmidt"])); // ["John", "Jacob", "Jingleheimer", "Schmidt"] */

  /* 
  let ar1 = [1, 2, 3, 4]
ar2 = ar1.push(5)
console.log(ar2) */

/* 
let ar1 = [1, 2, 3, 4]
let ar1Loop = ar1.map((value, i)=>{
    return (value)
})
console.log(ar1Loop)


 */


let arNumber = [1, 3, 5, 7, 9]