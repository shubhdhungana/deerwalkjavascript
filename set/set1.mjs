
// question: remove its duplicate value
let ar1 = [1, 2, 1, 3, 4]
console.log(ar1) //[ 1, 2, 3, 4 ]



//memorize it, memorize its syntax (to remove duplicate value)
let setUniqueValue = [...new Set(ar1)]; // memorize its syntax for interview
console.log(setUniqueValue) //[ 1, 2, 3, 4 ]


// console.log([...new Set(ar1)])