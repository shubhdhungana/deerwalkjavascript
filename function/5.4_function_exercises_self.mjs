/*child adult if else function */
/* let childAdult = function(age) {
    if (age < 18) {
        return("He/she is immature")
    }
    else {
        return("He/she is adult")
    }

}

let _childAdult = childAdult(19)
console.log(_childAdult)

 */



/* 
// sum default value function

let sumDefaultValue = function(a=1, b=1) {
    let c = `${a},${b}`
    return c
}

let _sumDefaultValue = sumDefaultValue(4)
console.log(_sumDefaultValue) */


/////////////////////////////////////

/* 
// difference function

let minusFunction = function(a,b) {
    let c = a - b
    return c
}

let _minusFunction = minusFunction(100,52)
console.log(`The minus result if ${_minusFunction}`)

 */

//////////////////////


/*
1. Define a function with one parameter. The function returns
 a string starting with "The given argument was: " and ends with
 ". Ok?". Insert the argument between those two strings. 
*/
/* let stringManipulation = function(parameter1) {
    return (`The given argument was: "${parameter1}".OK?".`)
}

let _stringManipulation = stringManipulation("hi sexy")
console.log(_stringManipulation)

/////////////////////////////////////
 */


///////////////////////////////////////
/* Define a function that takes two parameters.
 The function returns the average of both */

/* 
 let averageFunction = function(num1, num2) {
    let averageNumber = (num1 + num2)/2
    return averageNumber
 }

let _averageFunction = averageFunction(103, 102)
console.log(`The average value is ${_averageFunction}`)

 */

/////////////////////////////////
/*describe about car*/

/* let carDescribe = function(a,b,c="horsepower",d="color") {
    let description = `${a},${b},${c},${d}`
    return description
}

let _carDescribe = carDescribe("producer", "model")
console.log(_carDescribe)

/////////////////////////////////////////

 */

// function to return square of a number

/* let squareNumber = function(num) {
    return num*num
}

let _squareNumber = squareNumber(244)
console.log(`The square number is ${_squareNumber}`)

 */

//////////////////////////////////////////

/*

    Write a function named tellFortune that:
        takes 4 arguments: number of children, partner's name, geographic location, job title.
        outputs your fortune to the screen like so: "You will be a X in Y, and married to Z with N kids." 
    Call that function 3 times with 3 different values for the arguments. 

*/

 /* let tellFortune = function(numChild, loveName, location, job) {
    let output = `You'll be a ${job} in ${location}, and married to ${loveName} with ${numChild} kids`
    return output
}

let _tellFortune = tellFortune(4, "Subha", "USA", "CEO of Google")
let _tellFortune2 = tellFortune(3, "Subha", "South Carolina", "Facebook")
console.log(_tellFortune)
console.log(_tellFortune2)
 */

////////////////////////////


/* 
// isEven function to check odd and even

let isEven = function(num) {
    if(num % 2 === 0) {
        return (true)
    }
    else {
        return (false)
    }
}

let _isEven = isEven(6)
console.log(_isEven)  */