/*
Write a function that reverses the order of words in a sentence.
*/

let reverseSentence = (sentence)=>{
    let sentenceArr = sentence.split(" ") // changing into string
    return (sentenceArr.reverse().join(" ")) // reversing array->and joining
}

console.log(reverseSentence("this is a book"))


