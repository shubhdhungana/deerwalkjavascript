/*
if gender is male => He is male
if gender is female => He is female
if gender is other than male and female  => He is other
*/

let maleFemale = (gender)=>{
    if (gender === "male") {
        return ("he is male")
    }
    else {
        return ("She is female")
    }
}

console.log(maleFemale("male")) // he is male