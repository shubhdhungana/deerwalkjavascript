/* here, first the function give preference to its assigned value 
while calling it i.e. sum(1). And then it give preference
to other value at function (a=9, b=10) but the first preference
is given to value while calling function */

let sum = function(a = 9, b = 10) {
    console.log(a)
    console.log(b)
}
sum(1)
