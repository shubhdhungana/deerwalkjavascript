/*
make a function , pass one input as  ["smoking", "drinking", "biting nails"] and the function 
must return true if the given input has   smoking field

make function in one file and call in another
*/

export let checkSmoking = (arrSentence)=>{
    let smokingCheck = arrSentence.some((value, i)=>{
        if(value === "smoking") {
            return (true)
        }
    })
    return smokingCheck
}

