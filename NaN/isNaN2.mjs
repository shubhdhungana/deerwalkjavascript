// checking NaN 

console.log(isNaN(1))

// 1; false
// 2; false
// "ram" ==> true

// checking techniques

/*
Since performing 1 is not NaN, so true
since, performing "ram" can give Nan, so true
*/
// remember this
 /*
just say 1 is not a number  which is false because it is number
- also, say 2 is not a number which is false because it is number
- "ram" is not a number, which is true, because it is not number

 */