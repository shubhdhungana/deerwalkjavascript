let obj1 = {name: "nitan", age: 29}
let obj2 = {address:"gagal"}

let obj3 = {isMarried:false, ...obj1, ...obj2}
//=> { isMarried: false, name: 'nitan', age: 29, address: 'gagal' }


let obj4 = {isMarried:true, obj1, ...obj2}
//=> { isMarried: true, obj1: { name: 'nitan', age: 29 }, address: 'gagal' }

console.log(obj3)
console.log(obj4)

