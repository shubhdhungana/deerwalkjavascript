let bag1 = [1, 3, "book", "pencil"]
let bag2 = [3, 5, "torch", "tipex"]

// integrating bag1 and bag2
let bag3 = [...bag1, ...bag2]
console.log(bag3)


let bag4 = [...bag2, bag1] // [3, 5, "torch", "tipex", [1, 3, "book", "pencil"]]
console.log(bag4)