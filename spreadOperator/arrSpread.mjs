let basket1 = ["orange", "apple", "mango"]
let basket2 = ["pomegranate", "kissmiss"]

// spread operator (...) work is to open bracket or wrapper of array

let basket3 = [...basket1, "a", "b"] 
let basket4 = ["a", ...basket1, "b", basket2]
console.log(basket3) // => [ 'orange', 'apple', 'mango', 'a', 'b' ]
console.log(basket4) //=> [ 'a', 'orange', 'apple', 'mango', 'b', [ 'pomegranate', 'kissmiss' ] ]





