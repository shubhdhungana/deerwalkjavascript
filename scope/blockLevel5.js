{
    let a = 1
    {
        let a = 5
        console.log(a)
    }
    console.log(a)
}

/*
output: 
5
1
*/

// while calling variable
// First, it searches variable in its own block
// If doesn't found, searches on its respective parent block and so on
// this is called scope chaining