// first case
{
    let a = 1;
    console.log(a) // =>1
    // since let is there so it would be only recognized inside block
}
///////////////////////////////////


// second case
{
    let a = 1;
}
console.log(a) // ==> error
    // since let is being defined outside block, program doesn't recognize it.

///////////////////
// third case
{
    console.log(a) // error
    let a = 1;
    
    // a variable will be known inside block from the point where it is defined
}