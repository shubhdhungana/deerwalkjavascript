// first case
{
    let a =3
    {
        let a =7
        {
            a = 10
            console.log(a) // 10 since it changes parent block 'a' value to 10
        }
        console.log(a) // 10, since changed value is 'a' because of child effect

    }
    console.log(a) //3 since first block 'a' value is 3
}

// second case
{
    let a = 3;
    {
        let b = 7
        {
            a = 10
            console.log(a) // 10 since It changes first block a and takes value from it
        }
        console.log(a) // since changed 'a' value is 10
    }
    console.log(a) //since changed 'a' value is 10
}