let a = 3
let b = 9
{
    let a = 4
    {
        a = 5
    }
    console.log(a) // 5 since a has been changed
}
console.log(a) //3
console.log(b) //9

/*
output
5
3
9
*/