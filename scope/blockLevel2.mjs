// first case
{
    let a = 1 // no error

    {
       let a = 6 // no error
    }
}

// second case
{
    let a = 1 // error
    let a = 7  // error, since two container of same name is defined
 
}

// third case
{
    let a = 1;
    {
        let a = 5;
    }
}
// we can not define same variable in same block
// but we can define same variable in different block