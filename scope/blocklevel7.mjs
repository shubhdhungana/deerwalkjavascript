let a = 3 // this is global block

{
    let a = 4
    console.log(a) //4
}

console.log(a) // 3

/*
When you execute a program, it has two phases:
i) memory allocation (it is being done first)
ii) code execution (it is done after memory allocation)

*/