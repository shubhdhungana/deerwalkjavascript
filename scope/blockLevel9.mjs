let a = 3

{
    console.log(a) //error
    let a = 8
}

/*
console.log(a) results error because the variable will be searched in 
its own block (but scope of a is from line number 5). So the program will
look for a in its own block but it can't find the value. Hence, error will be 
occured. Since error will be occured, it won't look for 'a' in its parent block
*/

/////////////////////

let a = 3

{
    console.log(a) //3
    a = 8
}

/*
Here, output will be 3. In line 20, it will look for a in its block.
Since there is no a variable in  'a' block so it will look for its parent block
i.e. a = 3
*/
