
// scope chaining
{
    let a = 1;
    {
        console.log(a) // 1 since its block doesn't have any variable so it looks on parent block
    }

}

// while calling variable
// First, it searches variable in its own block
// If doesn't found, searches on its respective parent block and so on
// this is called scope chaining

console.log(1)