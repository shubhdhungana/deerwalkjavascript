{
    let a = 1;
    {
        let a = 5;
        console.log(a) // => 5, since it look within its block
    }
    console.log(a) // => 1, 1 is output since it looks within its block
}

